import React from 'react'
import ReactDOM from 'react-dom'
// import PrimeiroComponente from './componentes/PrimeiroComponente'
// import { CompA, CompB as B } from './componentes/DoisComponente'
// import MultiElementos from './componentes/MultiElementos'
// import FamiliaSilva from './componentes/FamiliaSilva'

// import Familia from './componentes/Familia'
// import Membro from './componentes/Membro'

// import ComponenteComFuncao from './componentes/ComponenteComFuncao'

// import Pai from './componentes/Pai'

// import ComponenteClasse from './componentes/ComponenteClasse'

// import Contador from './componentes/Contador'
import Hook from './componentes/Hook'

const elemento = document.getElementById('root')
// const hello = <h1>Olá React Texto</h1>
ReactDOM.render(
  <div>
    {/* <MultiElementos/> */}
    {/* <CompA valor="Hello A" />
    <B valor="Hello B" /> */}
    {/* <PrimeiroComponente valor="Bom dia!" TesTe={1992 * 50} /> */}
    {/* <FamiliaSilva /> */}

    {/* <Familia sobrenome="Pereira">
      <Membro nome="André" />
      <Membro nome="Mariana" sobrenome="Pereira" />
    </Familia> */}

    {/* <ComponenteComFuncao /> */}

    {/* <Pai /> */}

    {/* <ComponenteClasse valor="Meu componente" /> */}

    {/* <Contador numeroInicial={100} /> */}

    <Hook />

  </div>
  , elemento)