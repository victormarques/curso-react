const mongoose = require('mongoose')

// remove mensagem de advertência
mongoose.Promise = global.Promise

module.exports = mongoose.connect('mongodb://localhost/todo')
